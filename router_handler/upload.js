
// 上传文件
const uploadHandler = (req, res) => {
  if (!req.file) return res.cc('field:avatar is file and required！')
  const fileInfo = req.file
  fileInfo.path = `/uploads/${fileInfo.filename}`
  res.send({
    status: 0,
    fileInfo,
    msg: '上传成功！'
  })
}

module.exports = uploadHandler
