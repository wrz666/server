const bcrypt = require('bcryptjs') // 密码加密和解密
// const db = require('../db/index') // 导入数据库对象
const db = require('../utils/db')

// 修改密码
const updatePwd = async (req, res) => {
  const user_id = req.query.user_id
  const { currentPwd, editPwd, repeatPwd } = req.body
  // 查询
  const result = await db.selectData('users', 'user_id', user_id)
  const { data } = result
  if (result.status !== 0) return res.cc(result.err)
  if (data.length !== 1) return res.cc('用户不存在！')
  const compareResult = bcrypt.compareSync(currentPwd, data[0].password)
  if (!compareResult) return res.cc('当前密码错误！')
  if (currentPwd === editPwd) return res.cc('不能与原先密码一致！')
  // 更新密码
  const password = bcrypt.hashSync(repeatPwd, 10)
  const updateRes = await db.updateData('users', {password}, 'user_id', user_id)
  if (updateRes.status !== 0) return res.cc(updateRes.err)
  if (updateRes.data.affectedRows !== 1) return res.cc('修改失败，请重试！')
  res.send({
    status: 0,
    msg: '修改成功！'
  })
}

// 更新用户基本资料
const updateInfo = async (req, res) => {
  const user_id = req.query.user_id
  const userInfo = req.body
  // console.log(req.user);
  if (JSON.stringify(userInfo) === '{}') return res.cc('请填写要修改的参数！')

  // 查询
  const selectData = await db.selectData('users', 'user_id', user_id)
  if (selectData.status !== 0) return res.cc(selectData.err)
  if (selectData.data.length !== 1) return res.cc('该用户不存在！')
  // 更新
  const updateData = await db.updateData('users', userInfo, 'user_id', user_id)
  if (updateData.status !== 0) return res.cc(updateData.err)
  const {data: result} = updateData
  if (result.affectedRows !== 1) return res.cc('更新用户信息失败，请重试！')
  res.send({
    status: 0,
    msg: '修改成功！'
  })
}

// 获取用户列表
const getUserList = async (req, res) => {
  let {fuzzy, pageNum, pageSize, sortKey, sortValue} = req.query
  if (!pageNum) pageNum = 1
  if (!pageSize) pageSize = 10
  let totalSql = `select count(*) AS total from users` // 查询所有数据总条数
  let selectSql = `select user_id,username,nickname,user_pic,is_admin,is_use,reg_time from users` // 查询表数据
  let total = 0 // 总条数
  if (fuzzy) {
    // 查询用户名或昵称为fuzzy的用户列表
    totalSql += ` where username like '%${fuzzy}%' or nickname like '%${fuzzy}%'`
    selectSql += ` where username like '%${fuzzy}%' or nickname like '%${fuzzy}%'`
  }
  // 添加分页和排序 根据注册时间 添加limit分页
  if (sortKey && sortValue) selectSql += ` order by ${sortKey} ${sortValue} limit ${(pageNum - 1) * pageSize},${pageSize}`
  selectSql += ` limit ${(pageNum - 1) * pageSize},${pageSize}` // 不排序
  // 执行sql语句
  const totalRes = await db.selectDataBySql(totalSql)
  if (totalRes.status !== 0) return res.cc(totalRes.err)
  total = totalRes.data[0].total // 总条数
  const sqlectRes = await db.selectDataBySql(selectSql)
  if (sqlectRes.status !== 0) return res.cc(sqlectRes.err)
  const userList = sqlectRes.data
  res.send({
    status: 0,
    msg: '获取用户列表成功！',
    total,
    data: userList
  })
}
module.exports = {
  updatePwd,
  updateInfo,
  getUserList
}
