
const bcrypt = require('bcryptjs') // 密码加密和解密
// const db = require('../db/index') // 导入数据库对象
const db = require('../utils/db')
const dateFormat = require('../utils/dateFormat') // 时间格式化
const jwt = require('jsonwebtoken')
const { jwtConfig } = require('../config')
// 注册
const regUser = async (req, res) => {
  // 用户名密码校验正确
  const userInfo = req.body
  userInfo.reg_time = dateFormat(new Date(), 'yyyy-MM-dd hh:mm:ss')
  userInfo.password = bcrypt.hashSync(userInfo.password, 10) // 对密码加密
  const result = await db.addData('users', userInfo)
  if (result.status !== 0) {
    if (result.err.code === 'ER_DUP_ENTRY') {
      const arr = result.err.sqlMessage.split(' ')
      return arr[arr.length - 1] === "'username'" ? res.cc('用户名重复，换一个叭！') : res.cc(result.err)
    }
    return res.cc(result.err)
  }
  res.cc('注册成功！', 0)
}

// 登录
const login = async (req, res) => {
  const userInfo = req.body
  const sql = `select * from users where username = ?`
  // 查询用户在不在数据库中
  const result = await db.selectData('users', 'username', userInfo.username)
  if (result.status !== 0) return res.cc(result.err)
  if (result.data.length !== 1) return res.cc('登陆失败，查无此人！')
  const compareResult = bcrypt.compareSync(userInfo.password, result.data[0].password)
  if (!compareResult) return res.cc('密码错误，登录失败！')
  const user = { ...result.data[0], password: '' }
  const token = jwt.sign(user, jwtConfig.jwtKey, {expiresIn: jwtConfig.expiresIn})
  res.send({
    status: 0,
    msg: '登陆成功！',
    userInfo: user,
    token: 'Bearer ' + token
  })
}


module.exports = {
  regUser,
  login
}
