const express = require('express')
const app = express()
const serve = '' // 后续配置https时用
const joi = require('joi')
const expressJWT = require('express-jwt') // 解析token
const bodyParser = require('body-parser')
const createRouter = require('./utils/createRouter') // 挂载路由函数
// 定义输出错误状态的中间件
app.use((req, res, next) => {
  // 挂载函数，全局能用
  res.cc = function (err, status = 1) {
    res.send({
      status,
      msg: err instanceof Error ? err.message : err
    })
  }
  next()
})
// 处理表单数据
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// 在路由之前解析token 排除 api和uploads开头的路由
const { jwtConfig } = require('./config')
app.use(expressJWT({ secret: jwtConfig.jwtKey, algorithms: ['HS256'] }).unless({ path: [/^\/api/, /^\/uploads/] }))
// 托管静态资源
app.use('/uploads', express.static('./uploads'))

// 批量导入路由并挂载路由
createRouter(app, __dirname + '\\router', 'router')
// app.use('/api', userRouter)
// app.use('/my', userInfoRouter, uploadRouter)
// 定义错误级别中间件
app.use((err, req, res, next) => {
  if (err instanceof joi.ValidationError) return res.cc(err)
  // 捕获身份认证错误
	if (err.name === 'UnauthorizedError') {
		if (err.message === 'jwt expired') return res.cc('token过期，请重新登录！', 401)
    return res.cc('身份认证失败！', 401)
	}
  res.cc(err)
})

let port = 8086 // 端口
let protocol = serve ? 'https' : 'http' // 协议
app.listen(port, () => {
  console.log(`serve is running at ${protocol}//127.0.0.1:${port}`)
})
