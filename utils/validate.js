// 校验数据

// schemas 校验对象
const joi = require('joi')
const validateJoi = function (schemas) {
  // TODO: 用户指定了什么 schema，就应该校验什么样的数据
  return function (req, res, next) {
      ;['query', 'body', 'params'].forEach((key) => {
        // 如果当前循环的这一项 schema 没有提供，则不执行对应的校验
        if (!schemas[key]) return
        // 执行校验
        const schema = joi.object(schemas[key])
        const { error, value } = schema.validate(req[key])
        if (error) {
          // 校验失败
          throw error
        } else {
          // 校验成功，把校验的结果重新赋值到 req 对应的 key 上
          req[key] = value
        }
      })
    // 校验通过
    next()
  }
}

module.exports = validateJoi
