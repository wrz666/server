const db = require('../db/index') // 数据库对象
const tip = {
  status: 0,
  msg: '',
  data: null,
  error: null
}
/**
* 添加数据
* @param {String} storeName 表名
* @param {Object} data 要插入的数据
* @param
**/
const addData = function (storeName, data) {
  return new Promise((resolve, reject) => {
    const sql = `insert into ${storeName} set ?`
    db.query(sql, data, (err, res) => {
      if (err) {
        resolve({status: 1, msg: '插入数据失败', data: null, err})
      } else {
        resolve({status: 0, msg: '插入数据成功', data: res, err: null})
      }
    })
  })
}

/**
* 根据某个字段更新数据
* @param {String} storeName 表名
* @param {Object} data 要更新的数据
* @param {String} key 字段
* @param {String} value 字段值
**/
const updateData = function (storeName, data, key, value) {
  return new Promise((resolve, reject) => {
    const sql = `update ${storeName} set ? where ${key} = '${value}'`
    console.log(sql);
    db.query(sql, data, (err, res) => {
      if (err) {
        resolve({status: 1, msg: '更新数据失败', data: null, err})
      } else {
        resolve({status: 0, msg: '更新数据成功', data: res, err: null})
      }
    })
  })
}

/**
* 根据某个字段查询数据
* @param {String} storeName 表名
* @param {String} key 字段
* @param {String} value 字段值
**/
const selectData = function (storeName, key, value) {
  return new Promise((resolve, reject) => {
    const sql = !key ? `select * from ${storeName}` : `select * from ${storeName} where ${key} = '${value}'`
    console.log(sql);
    db.query(sql, (err, res) => {
      if (err) {
        resolve({status: 1, msg: '查询失败', data: null, err})
      } else {
        resolve({status: 0, msg: '查询成功', data: res, err: null})
      }
    })
  })
}

/**
* 通过sql语句查询
* @param {String} sql sql语句
**/
const selectDataBySql = function (sql) {
  return new Promise((resolve, reject) => {
    db.query(sql, (err, res) => {
      if (err) {
        resolve({status: 1, msg: 'sql语句查询失败', data: null, err})
      } else {
        resolve({status: 0, msg: 'sql语句查询成功', data: res, err: null})
      }
    })
  })
}
module.exports = {
  addData,
  updateData,
  selectData,
  selectDataBySql
}
