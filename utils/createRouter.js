const fs = require('fs')

/**
* 挂载路由函数
* @param {object} app serve实例
* @param {string} path 路由文件所在目录绝对路径
* @param {string} dir 路由文件所在目录
*/
const cerateRouter = (app, path, dir) => {
  const routerDir = fs.readdirSync(dir) // 所有的路由文件名称组成的数组
  routerDir.forEach(item => {
    const url = path + '\\' + item
    const router = require(url)
    item.startsWith('api') ? app.use('/api', router) : app.use('/my', router)
  })
}
module.exports = cerateRouter
