/*
*@params date {Date Object} 日期
*@params fmt {String} 格式化方式
*@params bool {Boolean} 是否返回周几
*/
function dateFormat(date, fmt, bool) {
  date = new Date(date)
  var o = {
      "M+": date.getMonth() + 1, //月份
      "d+": date.getDate(), //日
      "h+": date.getHours(), //小时
      "m+": date.getMinutes(), //分
      "s+": date.getSeconds(), //秒
      "q+": Math.floor((date.getMonth() + 3) / 3), //季度
      S: date.getMilliseconds(), //毫秒
  }
  let weekStr = "" // 星期几
  const weekList = ['周日', '周一', '周二', '周三', '周四', '周五', '周六']
  weekStr = weekList[date.getDay()]
  if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length))
  }

  for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
          fmt = fmt.replace(
              RegExp.$1,
              RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length)
          )
      }
  }
  if (bool) fmt = fmt + " " + weekStr
  return fmt
}
module.exports = dateFormat
// console.log(dateFormat(+new Date(), "yyyy-MM-dd hh:mm:ss", 1))
