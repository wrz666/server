// 用户相关的字段校验
const joi = require('joi')

const user_id = joi.number().integer().min(1).required() // 用户id检验

// 注册登录
const reg_login = {
  body: {
    username: joi.string().alphanum().min(1).max(10).required(),
    password: joi.string().pattern(/^[\S]{6,12}$/).required()
  }
}

// 修改密码
const updatePwd = {
  query: {
    user_id
  },
  body: {
    currentPwd: joi.string().pattern(/^[\S]{6,12}$/).required(),
    editPwd: joi.string().pattern(/^[\S]{6,12}$/).required(),
    repeatPwd: joi.ref('editPwd')
  }
}
// 修改用户基本信息
const updateInfo = {
  query: {
    user_id
  },
  body: {
    nickname: joi.string().max(10).disallow(''),
    user_pic: joi.string().allow(''),
    is_admin: joi.number().integer().valid(0, 1),
    is_use: joi.number().integer().valid(0, 1)
  }
}
// 获取用户列表
const getUser = {
  query: {
    fuzzy: joi.string().allow(''),
    pageNum: joi.number().integer().min(1).disallow(''),
    pageSize: joi.number().integer().min(1).disallow(''),
    sortKey: joi.string().disallow(''),
    sortValue: joi.string().disallow('')
  }
}
module.exports = {
	reg_login,
  updatePwd,
  updateInfo,
  getUser
}
