const { Router } = require('express')
const router = Router()
const validateJoi = require('../utils/validate') // 校验数据
const userSchema = require('../schema/user')
const { updatePwd, updateInfo, getUserList } = require('../router_handler/userinfo')
// 修改密码
router.put('/updatePwd', validateJoi(userSchema.updatePwd), updatePwd)

// 更新基本用户信息
router.put('/updateInfo', validateJoi(userSchema.updateInfo), updateInfo)

// 获取用户列表
router.get('/getUserListPage', validateJoi(userSchema.getUser), getUserList)
module.exports = router
