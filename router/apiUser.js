// 用户相关的路由
const { Router } = require('express')
const router = Router()
const validateJoi = require('../utils/validate') // 校验数据
// 路由处理函数
const user_handler = require('../router_handler/user')
const userSchema = require('../schema/user') // 用户相关的校验对象
// 用户注册
router.post('/reguser', validateJoi(userSchema.reg_login), user_handler.regUser)

// 用户登录
router.post('/login', validateJoi(userSchema.reg_login), user_handler.login)

module.exports = router
