const { Router } = require('express')
const router = Router()
const dateFormat = require('../utils/dateFormat')
const uploadHandler = require('../router_handler/upload')
const multer = require('multer') // 上传文件所需要的包


// 设置文件存储的格式，以及位置
const storage = multer.diskStorage({
  destination: function (req, file, cb) { // 设置文件存储的位置
    cb(null, './uploads')
  },
  filename: function (req, file, cb) { // 设置文件名的格式
    // 获取文件后缀名
    const prefix = file.originalname.split('.')[file.originalname.split('.').length - 1]
    cb(null, file.fieldname + '-' + dateFormat(new Date(), 'yyyy-MM-dd hh-mm-ss') + '.' + prefix)
  }
})
// 设置文件过滤器 用来过滤不符合条件的文件
const fileFilter = (req, file, cb) => {
  // 设置文件格式
  const type = ['image/png', 'image/jpeg', 'image/jpg', 'image/gif']
  if (type.includes(file.mimetype)) return cb(null, true)
  cb(new Error('上传失败，不支持的文件格式'+file.mimetype), false)
}

const upload = multer({
  storage,
  fileFilter,
})
// 上传文件
router.post('/uploads', upload.single('avatar'), uploadHandler)
module.exports = router
