# server

#### 介绍
node项目实战api接口：登录，注册，获取用户列表，文件上传等

#### 软件架构
node + express


#### 安装教程

```bash
git clone https://gitee.com/wrz666/server.git
cd server
npm i
nodemon app.js  或者  node app.js
```

#### 接口文档
[api接口文档](https://www.wrz521.top/archives/90/)
